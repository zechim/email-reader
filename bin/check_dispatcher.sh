#!/bin/bash

dispatcher=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
dispatcher="$dispatcher/dispatcher.rb"

if ps ax | grep -v grep | grep dispatcher.rb > /dev/null
then
    exit
else
    `/usr/local/bin/ruby $dispatcher >> /var/log/ereader_dispatcher.log 2>&1 &` 
fi
