#!/usr/bin/env php
<?php

require_once __DIR__ . '/../bootstrap.php';

use EReader\Core\Fetcher\Dispatcher;
use EReader\Core\Fetcher\Output;
use EReader\Core\Fetcher\DispatcherConfig;

$app['db.orm.em']->getRepository('EReader\Entity\EREmail')
    			 ->cleanUp($app['config']['maxSecondsExecutionTime']);

$output    = new Output();
$erEmailId = isset($argv) ? $argv : array();
$erEmailId = isset($erEmailId[1]) ? $erEmailId[1] : null;

$dispatcherConfig = new DispatcherConfig();
$dispatcherConfig->setErEmailId($erEmailId);
$dispatcherConfig->setAttachmentPath($app['config']['attachmentPath']);
$dispatcherConfig->setHashRegex($app['config']['hashRegex']);
$dispatcherConfig->setMessageLimit($app['config']['messageLimit']);
$dispatcherConfig->setPdfPath($app['config']['pdfPath']);
$dispatcherConfig->setPdfUrl($app['config']['pdfUrl']);

try {
    $dispatcher = new Dispatcher($app['db.orm.em'], $output);
    $dispatcher->dispatch($dispatcherConfig);
    
} catch (\Exception $e) {
    $output->write($e->getMessage());
} 