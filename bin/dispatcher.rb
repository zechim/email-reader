require 'mysql2'
require 'yaml'
config = YAML.load(File.read(File.dirname(__FILE__) + '/../config.yml'))

def createConn(config)
  return Mysql2::Client.new :host => config['database']['host'], :username => config['database']['user'], :password => config['database']['password'], :database => config['database']['dbname']
end

db       = createConn(config)
dispatch = File.dirname(__FILE__) + '/dispatcher.php';

while(true)
  begin
    db.query("SELECT e.id FROM easy_pabx_serv_fila_universal_mail_conf e WHERE e.status = 'WAITING' and active = 1").each do |result|
      Thread.new {
	 	str = `php #{dispatch} #{result['id']}`
	 	str.split("\n").each do|c|
          puts c
        end
      }
    end
	rescue Exception => e
      puts e.message
      db = createConn(config)
  end
  sleep(1)
end
