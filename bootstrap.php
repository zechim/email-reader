<?php

require_once __DIR__ . '/vendor/autoload.php';

use Silex\Provider\DoctrineServiceProvider;
use Symfony\Component\Yaml\Yaml;
use EReader\Core\Doctrine\DoctrineORMServiceProvider;

$app = new Silex\Application();
$app['config'] = Yaml::parse(__DIR__ . '/config.yml');
$app['debug']  = true;

$app->register(new DoctrineServiceProvider(), array(
    'dbs.options' => array ('mysql' => $app['config']['database']) 
));
$app->register(new DoctrineORMServiceProvider(), array(
    'db.orm.proxies_dir'           => __DIR__ . '/cache/doctrine/Proxy',
    'db.orm.proxies_namespace'     => 'DoctrineProxy',
    'db.orm.auto_generate_proxies' => true,
    'db.orm.entities'              => array(array(
        'type'      => 'yml',
        'path'      => __DIR__ . '/src/EReader/yml',
        'namespace' => 'EReader\Entity',
    )),
));
