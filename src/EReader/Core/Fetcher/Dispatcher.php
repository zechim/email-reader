<?php 

namespace EReader\Core\Fetcher;

use Doctrine\ORM\EntityManager;
use EReader\Core\Fetcher\Factory\FetcherFactory;
use EReader\Entity\EREmail;
class Dispatcher
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;
    
    /**
     * @var Output
     */
    protected $output;
    
    public function __construct(EntityManager $em, Output $output)
    {
        $this->em     = $em;
        $this->em     = $em;
        $this->output = $output;
    }
    
    protected function isRunning()
    {
        $cmd = explode("\n", shell_exec("ps p " . getmypid() . '  o cmd'));

        if (isset($cmd[1])) {
            if (count(explode("\n", shell_exec('ps -ef | grep -E "' . trim($cmd[1]) . '"'))) > 4) {
                return true;
            }
        }
        
        return false;
    }
    
    public function dispatch(DispatcherConfig $dispatcherConfig)
    {
        if ($this->isRunning()) {
            return false;
        }
        
        $erEmail = $this->findEREmail($dispatcherConfig->getErEmailId());
        
        try {
            $emailFactory = new FetcherFactory($this->em, $this->output);
            $emailFactory->fetch($erEmail, $dispatcherConfig);
            
        } catch (\Exception $e) {
            echo $e->getMessage();
            $this->em->getRepository('EReader\Entity\EREmail')->setException($erEmail, $e->getMessage());
            
            throw $e;
        }
    }
    
    /**
     * @param intger $id
     * @return \EReader\Entity\EREmail
     */
    protected function findEREmail($id)
    {
        $erEmail = $this->em->getRepository('EReader\Entity\EREmail')->findOneBy(array('id' => $id));
        
        if (!$erEmail instanceof EREmail) {
            throw new \InvalidArgumentException(sprintf('Email not found for id "%d"', $id));
        }
        
        if ($erEmail->getActive() == EREmail::ACTIVE_NO) {
            throw new \InvalidArgumentException(sprintf('Account "%s" at "%s" is disabled', $erEmail->getUser(), $erEmail->getAccount()));
        }
        
        return $erEmail;
    } 
}

