<?php 

namespace EReader\Core\Fetcher;

class DispatcherConfig
{
    protected $erEmailId;
    protected $attachmentPath;
    protected $hashRegex;
    protected $messageLimit;
    protected $pdfPath;
    protected $pdfUrl;
    
	/**
     * @return the $messageLimit
     */
    public function getMessageLimit()
    {
        return $this->messageLimit;
    }

	/**
     * @param field_type $messageLimit
     */
    public function setMessageLimit($messageLimit)
    {
        $this->messageLimit = $messageLimit;
    }

	/**
     * @return the $erEmailId
     */
    public function getErEmailId()
    {
        return $this->erEmailId;
    }

	/**
     * @return the $attachmentPath
     */
    public function getAttachmentPath()
    {
        return $this->attachmentPath;
    }

	/**
     * @return the $hashRegex
     */
    public function getHashRegex()
    {
        return $this->hashRegex;
    }

	/**
     * @param field_type $erEmailId
     */
    public function setErEmailId($erEmailId)
    {
        $this->erEmailId = $erEmailId;
    }

	/**
     * @param field_type $attachmentPath
     */
    public function setAttachmentPath($attachmentPath)
    {
        $this->attachmentPath = $attachmentPath;
    }

	/**
     * @param field_type $hashRegex
     */
    public function setHashRegex($hashRegex)
    {
        $this->hashRegex = $hashRegex;
    }
    
	/**
     * @return the $pdfUrl
     */
    public function getPdfUrl()
    {
        return $this->pdfUrl;
    }

	/**
     * @param field_type $pdfUrl
     */
    public function setPdfUrl($pdfUrl)
    {
        $this->pdfUrl = $pdfUrl;
    }
    
	/**
     * @return the $pdfPath
     */
    public function getPdfPath()
    {
        return $this->pdfPath;
    }

	/**
     * @param field_type $pdfPath
     */
    public function setPdfPath($pdfPath)
    {
        $this->pdfPath = $pdfPath;
    }



    
    
}

