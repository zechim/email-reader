<?php 

namespace EReader\Core\Fetcher\Factory;

use Doctrine\ORM\EntityManager;
use EReader\Core\Fetcher\Output;
use EReader\Entity\EREmail;
use EReader\Core\Fetcher\DispatcherConfig;
use EReader\Core\Protocol\ImapConnection;
use EReader\Core\Protocol\Imap;

class FetcherFactory
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    
    /**
     * @var \EReader\Core\Fetcher\Output
     */
    protected $output;
    
    public function __construct(EntityManager $em, Output $output)
    {
        $this->em     = $em;
        $this->output = $output;
    }
    
    public function fetch(EREmail $erEmail, DispatcherConfig $dispatcherConfig)
    {
        $this->em->getRepository('EReader\Entity\EREmail')->beginFetch($erEmail);
        
        $fetchedAt = new \DateTime;
        
        $this->output->write(sprintf('Fetching "%s" account for "%s" user', $erEmail->getAccount(), $erEmail->getUser()));
        
        $imapConnection = new ImapConnection($erEmail->getHostname(), $erEmail->getUser(), $erEmail->getPass());
        $imap = new Imap($imapConnection);
        
        $repo = $this->em->getRepository('EReader\Entity\ERMessage');
        $t = 0;
        
        /* @var $message \EReader\Core\Protocol\ImapMessage */
        foreach ($imap->search() as $message) {
            $t++;

            $repo->saveMessage($message, $erEmail, $dispatcherConfig);
            
            $imap->setAsSeen($message->getMessageNumber());
            
            if ($t == $dispatcherConfig->getMessageLimit()) {
                break;
            }
        } 

        $this->output->write(sprintf('"%d" message(s) found for "%s" user', $t, $erEmail->getUser()));

        $this->em->getRepository('EReader\Entity\EREmail')->finishFetch($erEmail, $t, $fetchedAt);
    }
}