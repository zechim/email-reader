<?php 

namespace EReader\Core\Fetcher;

class Output
{
    public function __construct()
    {
        ob_start();
    }
    
    public function write($msg)
    {
        printf('[%s] [memory %s] [at %s] %s', $msg, $this->formatBytes(memory_get_usage()), date('d-m-Y H:i:s'), PHP_EOL);
        ob_flush();
        flush();
    }
    
    protected function formatBytes($size, $precision = 2)
    {
        $base = log($size) / log(1024);
        $suffixes = array('', 'k', 'M', 'G', 'T');

        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    }   
}