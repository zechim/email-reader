<?php 

namespace EReader\Core\Protocol;

class Imap
{
    /**
     * @var ImapConnection
     */
    protected $imapConnection;

    /**
     * @param ImapConnection $connection
     */
    public function __construct(ImapConnection $imapConnection)
    {
        $this->imapConnection = $imapConnection;
    }

    public function getResource()
    {
        return $this->imapConnection->getResource();
    }

    /**
     * @return ImapMessageIterator
     */
    public function search($flag = 'UNSEEN')
    {
        $messages = imap_search($this->getResource(), $flag, SE_UID);
        $messages = is_array($messages) ? $messages : array();

        return new ImapMessageIterator($this, $messages);
    }

    public function fetchStructure($messageNumber)
    {
        return imap_fetchstructure($this->getResource(), $messageNumber, FT_UID);
    }

    public function fetchBody($messageNumber, $partNumber = null)
    {
        return imap_fetchbody($this->getResource(), $messageNumber, $partNumber ?: 1, FT_UID | FT_PEEK);
    }

    public function setAsSeen($messageNumber)
    {
        return imap_setflag_full($this->getResource(), $messageNumber, '\Seen \Flagged', ST_UID);
    }

    public function fetchHeader($messageNumber)
    {
        return imap_rfc822_parse_headers(imap_fetchheader($this->getResource(), $messageNumber, FT_UID));
    }

    public function disconnect()
    {
        return $this->imapConnection->disconnect();
    }
}