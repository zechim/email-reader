<?php 

namespace EReader\Core\Protocol;

class ImapConnection
{
    protected $resource;
    
    public function __construct($host, $user, $pass)
    {
        $resource = @imap_open($host, $user, $pass);
        
        $error = imap_errors();
        $error = isset($error[0]) ? $error[0] : 'Connection problem';
        
        imap_alerts();
                
        if ($resource == false) {
            throw new \Exception($error);    
        }
        
        $this->resource = $resource; 
    }
    
    public function getResource()
    {
        return $this->resource;
    }
    
    public function disconnect()
    {
        return imap_close($this->resource, CL_EXPUNGE);
    }
}