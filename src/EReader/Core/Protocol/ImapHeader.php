<?php 

namespace EReader\Core\Protocol;

class ImapHeader
{
    protected $fromName;
    protected $fromEmail;
    protected $subject;
    protected $date;
    
    public function __construct(Imap $imap, $messageNumber)
    {
        $header = array_change_key_case((array) $imap->fetchHeader($messageNumber));
        
        $this->setFromName($header);
        $this->setFromEmail($header);
        $this->setSubject($header);
        $this->setDate($header);
    }
    
    protected function setFromEmail(array $header)
    {
        $fromEmail = '';
           
        if (array_key_exists('from', $header)) {
            $fromEmail = current($header['from']);
            $fromEmail = sprintf('%s@%s', $fromEmail->mailbox, $fromEmail->host);
        }

        $this->fromEmail = $fromEmail;
    }
    
    protected function setFromName(array $header)
    {
        $fromName = '';
        
        if (array_key_exists('from', $header)) {
            $fromName = current($header['from']);
            $fromName = isset($fromName->personal) ? $fromName : '';
            
            if ($fromName == '') {
                $this->fromName = $fromName;
                return;
            }

            $fromName = current(imap_mime_header_decode($fromName->personal));
            $charset  =  $fromName->charset == 'default' ? 'ISO-8859-1' : $fromName->charset;
            $fromName = iconv($charset, 'UTF-8', $fromName->text);
        }
        
        $this->fromName = $fromName;
    }
    
    protected function setDate(array $header)
    {
        $date = date('Y-m-d H:i:s');
        
        if (array_key_exists('date', $header)) {
            $date = preg_replace('/(.*)\(.*\)/', '$1', $header['date']);
            $date = strtotime($date);
            $date = $date ? date('Y-m-d H:i:s', $date) : date('Y-m-d H:i:s');
        }
        
        $date = explode(' ', $date);
        
        if (count($date) !== 2) {
            $this->date = new \DateTime;
            
            return;
        }
        
        $date[0] = explode('-', $date[0]);
        
        if (count($date[0]) !== 3) {
            $this->date = new \DateTime;
            
            return;
        }

        $date[1] = explode(':', $date[1]);
        
        if (count($date[1]) !== 3) {
            $this->date = new \DateTime;
        
            return;
        }
        
        $this->date = new \DateTime(sprintf('%s %s', implode('-', $date[0]), implode(':', $date[1])));
    }
    
    protected function setSubject(array $header)
    {
        $subject = '';
        if (array_key_exists('subject', $header)) {
            $subject = '';
            foreach (imap_mime_header_decode($header['subject']) as $part) {
                $charset = $part->charset == 'default' ? 'ISO-8859-1' : $part->charset;
                $subject.= iconv($charset, 'UTF-8', $part->text);
            }
        }
        
        $this->subject = $subject;    
    }
    
	/**
     * @return the $fromName
     */
    public function getFromName()
    {
        return $this->fromName;
    }

	/**
     * @return the $fromEmail
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

	/**
     * @return the $subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

	/**
     * @return the $date
     */
    public function getDate()
    {
        return $this->date;
    }
}