<?php 

namespace EReader\Core\Protocol;

class ImapMessage extends ImapMessagePart
{
    /**
     * @var ImapHeader
     */
    protected $header;
    
    public function __construct(Imap $imap, $messageNumber)
    {
        $this->imap = $imap;
        $this->messageNumber = $messageNumber;

        $this->parse();
    }
    
    public function getHeader()
    {
        if ($this->header == null) {
            $this->header = new ImapHeader($this->imap, $this->messageNumber);
        }
        
        return $this->header;
    }
    
    public function savePDF($pdfPath, $pdfUrl)
    {
        $filename = uniqid() . time();
        $pdfFile = $pdfPath . $filename . '.pdf';

        $fp = fopen(($htmlFile = $pdfPath . $filename . '.html'), 'w+');
        
        fwrite($fp, $this->getBodyHtml());
        fclose($fp);
        
        chmod($htmlFile, 0775);
        
        $htmlUrl = $pdfUrl . $filename . '.html';
        
        system("wkhtmltopdf -q --encoding UTF-8 -s A4 $htmlUrl $pdfFile");
        
        @unlink($htmlFile);
        
        chmod($pdfFile, 0775);
        
        return $filename . '.pdf';
    }
}