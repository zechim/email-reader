<?php 

namespace EReader\Core\Protocol;

class ImapMessageAttachment extends ImapMessagePart
{
    public function getFilename()
    {
        return $this->parameters->offsetExists('filename') ? $this->parameters->offsetGet('filename') : 'unknown';
    }

    public function getSize()
    {
        return $this->parameters->offsetExists('size') ? $this->parameters->offsetGet('size') : 0;
    }
    
    public function saveFile($path)
    {
        $_path = trim($path, '/\\');
    
        $path = DIRECTORY_SEPARATOR . $_path;
        $path.= DIRECTORY_SEPARATOR . date('Y');
        $path.= DIRECTORY_SEPARATOR . date('m');
        $path.= DIRECTORY_SEPARATOR . date('d');
        $path.= DIRECTORY_SEPARATOR;
    
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
    
        $filename = $path . uniqid() . '_' . $this->slug($this->getFilename());
    
        file_put_contents($filename, $this->getDecodedContent());
    
        return trim(str_replace($_path . DIRECTORY_SEPARATOR, '', $filename), DIRECTORY_SEPARATOR);
    }
    
    protected function slug($string, $slug = '_', $extra = '.')
    {
        return strtolower(trim(preg_replace('~[^0-9a-z' . preg_quote($extra, '~') . ']+~i', $slug, $this->unaccent($string)), $slug));
    }
    
    protected function unaccent($string)
    {
        if (strpos($string = htmlentities($string, ENT_QUOTES, 'UTF-8'), '&') !== false) {
            $string = html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|tilde|uml);~i', '$1', $string), ENT_QUOTES, 'UTF-8');
        }
    
        return $string;
    }
}