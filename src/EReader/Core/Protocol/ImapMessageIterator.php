<?php 

namespace EReader\Core\Protocol;

class ImapMessageIterator extends \ArrayIterator
{
    /**
     * @var Imap $imap
     */
    protected $imap;
    
    public function __construct(Imap $imap, array $messages)
    {
        parent::__construct($messages);
        
        $this->imap = $imap;
    }
    
    public function current()
    {
        return new ImapMessage($this->imap, parent::current());
    }
}