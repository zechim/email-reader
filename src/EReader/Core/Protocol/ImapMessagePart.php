<?php 

namespace EReader\Core\Protocol;

class ImapMessagePart implements \RecursiveIterator
{
    const TYPE_TEXT = 'text';
    const TYPE_MULTIPART = 'multipart';
    const TYPE_MESSAGE = 'message';
    const TYPE_APPLICATION = 'application';
    const TYPE_AUDIO = 'audio';
    const TYPE_IMAGE = 'image';
    const TYPE_VIDEO = 'video';
    const TYPE_OTHER = 'other';

    const ENCODING_7BIT = '7bit';
    const ENCODING_8BIT = '8bit';
    const ENCODING_BINARY = 'binary';
    const ENCODING_BASE64 = 'base64';
    const ENCODING_QUOTED_PRINTABLE = 'quoted-printable';
    const ENCODING_UNKNOWN = 'unknown';

    const SUBTYPE_TEXT = 'TEXT';
    const SUBTYPE_HTML = 'HTML';

    protected $typesMap = array(
        0 => self::TYPE_TEXT,
        1 => self::TYPE_MULTIPART,
        2 => self::TYPE_MESSAGE,
        3 => self::TYPE_APPLICATION,
        4 => self::TYPE_AUDIO,
        5 => self::TYPE_IMAGE,
        6 => self::TYPE_VIDEO,
        7 => self::TYPE_OTHER
    );

    protected $encodingsMap = array(
        0 => self::ENCODING_7BIT,
        1 => self::ENCODING_8BIT,
        2 => self::ENCODING_BINARY,
        3 => self::ENCODING_BASE64,
        4 => self::ENCODING_QUOTED_PRINTABLE,
        5 => self::ENCODING_UNKNOWN
    );
    
    /**
     * @var Imap
     */
    protected $imap;

    protected $type;
    protected $subtype;
    protected $encoding;
    protected $bytes;
    protected $lines;
    protected $parameters;
    protected $messageNumber;
    protected $partNumber;
    protected $structure;
    protected $content;
    protected $decodedContent;
    protected $parts = array();
    protected $key = 0;
    protected $disposition;
    protected $attachments;
    
    /**
     * Constructor
     *
     * @param \stdClass $part   The part
     * @param string $number The part number
     */
    public function __construct(Imap $imap, $messageNumber, $partNumber = null, $structure = null)
    {
        $this->imap = $imap;
        $this->messageNumber = $messageNumber;
        $this->partNumber = $partNumber;
    
        $this->parse($structure);
    }
    
    public function getAttachments()
    {
        $attachments = array();
        
        if (null === $this->attachments) {
            foreach ($this->getParts() as $part) {
                if ($part instanceof ImapMessageAttachment) {
                    $attachments[] = $part;
                }
                
                if($part->hasChildren()) {
                    foreach($part->getParts() as $childPart) {
                        if ($childPart instanceof ImapMessageAttachment) {
                            $attachments[] = $childPart;
                        }
                    }
                }
            }
        }
    
        return $this->attachments = $attachments;
    }
    
    protected function parse(\stdClass $structure = null)
    {
        $structure = $structure ? $structure : $this->imap->fetchStructure($this->messageNumber);
        
        $this->type = $this->typesMap[$structure->type];
        $this->encoding = $this->encodingsMap[$structure->encoding];
        $this->subtype = strtolower($structure->subtype);
        
        if (isset($structure->bytes)) {
            $this->bytes = $structure->bytes;
        }
        
        foreach (array('disposition', 'bytes', 'description') as $optional) {
            if (isset($structure->$optional)) {
                $this->$optional = $structure->$optional;
            }
        }
        
        $this->parameters = new \ArrayObject();
        foreach ($structure->parameters as $parameter) {
            $this->parameters->offsetSet(strtolower($parameter->attribute), $parameter->value);
        }
        
        if (isset($structure->dparameters)) {
            foreach ($structure->dparameters as $parameter) {
                $this->parameters->offsetSet(strtolower($parameter->attribute), $parameter->value);
            }
        }
        
        if (isset($structure->parts)) {
            foreach ($structure->parts as $key => $partStructure) {
                if (null === $this->partNumber) {
                    $partNumber = ($key + 1);
                } else {
                    $partNumber = (string) ($this->partNumber . '.' . ($key+1));
                }
                
                if (isset($partStructure->disposition)
                    && (strtolower($partStructure->disposition) == 'attachment' || strtolower($partStructure->disposition) == 'inline' )
                    //&& strtolower($partStructure->subtype) != "plain"
                    ) {
                    
                    $this->parts[] = new ImapMessageAttachment($this->imap, $this->messageNumber, $partNumber, $partStructure);
                    
                } else {
                    $this->parts[] = new ImapMessagePart($this->imap, $this->messageNumber, $partNumber, $partStructure);
                }
            }
        }
    }
    
    public function getParts()
    {
        return $this->parts;
    }
    
    public function current()
    {
        return array_key_exists($this->key, $this->parts) ? $this->parts[$this->key] : array();
    }
    
    public function getChildren()
    {
        return $this->current();
    }
    
    public function hasChildren()
    {
        return count($this->parts) > 0;
    }
    
    public function key()
    {
        return $this->key;
    }
    
    public function next()
    {
        ++$this->key;
    }
    
    public function rewind()
    {
        $this->key = 0;
    }
    
    public function valid()
    {
        return isset($this->parts[$this->key]);
    }
    
    public function getDisposition()
    {
        return $this->disposition;
    }
    
    public function getCharset()
    {
        return $this->parameters->offsetExists('charset') ? $this->parameters->offsetGet('charset') : '';
    }
    
    public function getType()
    {
        return $this->type;
    }
    
    public function getSubtype()
    {
        return $this->subtype;
    }
    
    public function getEncoding()
    {
        return $this->encoding;
    }
    
    public function getBytes()
    {
        return $this->bytes;
    }
    
    public function getLines()
    {
        return $this->lines;
    }
    
    public function getParameters()
    {
        return $this->parameters;
    }
    
    /**
     * Get raw part content
     *
     * @return string
     */
    public function getContent()
    {
        if (null === $this->content) {
            $this->content = $this->doGetContent();
        }
    
        return $this->content;
    }
    
    /**
     * Get decoded part content
     *
     * @return string
     */
    public function getDecodedContent()
    {        
        if (null === $this->decodedContent) {
            switch ($this->getEncoding()) {
                case self::ENCODING_BASE64:
                    $this->decodedContent = base64_decode($this->getContent());
                    break;
    
                case self::ENCODING_QUOTED_PRINTABLE: 
                case self::ENCODING_7BIT:
                case self::ENCODING_8BIT:
                    $this->decodedContent = $this->getContent();
                    break;
    
                default:
                    throw new \UnexpectedValueException('Cannot decode ' . $this->getEncoding());
            }
    
            if ($this->getType() === self::TYPE_TEXT | $this->getType() === self::TYPE_MULTIPART) {
                $this->decodedContent = quoted_printable_decode($this->decodedContent);
            }
            
            if ($this->getType() === self::TYPE_TEXT
                && null !== $this->getCharset()
                && strtolower($this->getCharset()) != 'utf-8') {
                $this->decodedContent = \iconv($this->getCharset(), 'UTF-8', $this->decodedContent);
            }
            
            if ($this->getCharset() != 'utf-8') {
                $this->decodedContent = utf8_decode($this->decodedContent);
                
                if (($test = @iconv("UTF-8//TRANSLIT", "ISO-8859-1", $this->decodedContent)) !== false) {
                    $this->decodedContent = $test;
                }
                
                if ($this->getSubtype() == 'plain' && $this->getCharset() != 'utf-8') {
                    $this->decodedContent = utf8_encode($this->decodedContent);
                }
            }
        }
    
        return $this->decodedContent;
    }
    
    public function getStructure()
    {
        return $this->structure;
    }
        
    protected function doGetContent()
    {
        return $this->imap->fetchBody($this->messageNumber, $this->partNumber);
    }
    
    public function getBodyHtml()
    {
        $iterator = new \RecursiveIteratorIterator($this, \RecursiveIteratorIterator::SELF_FIRST);

        foreach ($iterator as $part) {
            if ($part->getSubtype() == 'html') {
                return $part->getDecodedContent();
            }
        }
        
        return $this->getBodyText();
    }
    
    public function getBodyText()
    {
        $iterator = new \RecursiveIteratorIterator($this, \RecursiveIteratorIterator::SELF_FIRST);
        
        foreach ($iterator as $part) {
            if ($part->getSubtype() == 'plain') {
                return $part->getDecodedContent();
            }
        }

        return $this->getDecodedContent();
    }
    
    public function getMessageNumber()
    {
        return $this->messageNumber;
    }
}   