<?php 

namespace EReader\Core\XML;

use EReader\Entity\ERMessage;

class MessageFetcher
{
    protected $messages = array();
    protected $doc;
    protected $rootTag;
    
    public function __construct($messages, array $config)
    {
        $this->messages = $messages;

        $this->doc     = new \DOMDocument('1.0', 'UTF-8');
        $this->rootTag = $this->doc->createElement('r');
        
        $totalMessages = $this->doc->createAttribute('t');
        
        $this->rootTag->appendChild($totalMessages);
        
        $this->doc->appendChild($this->rootTag);
        
        $t = 0;
        foreach ($messages as $message) {
            $t++;
            $this->addMessageToResponse($message, $config['attachmentUrl'], $config['pdfUrl']);
        }
        
        $totalMessages->value = $t;
    }
    
    protected function addMessageToResponse(ERMessage $message, $attachmentUrl, $pdfUrl)
    {
        $messageTag = $this->doc->createElement('m');
        
        $byAttribute = $this->doc->createAttribute('u');
        $byAttribute->value = $message->getFetchedBy();
        
        $idAttribute = $this->doc->createAttribute('id');
        $idAttribute->value = $message->getId();
        
        $messageTag->appendChild($byAttribute);
        $messageTag->appendChild($idAttribute);
        
        $this->appendWithCData('f', $message->getFromEmail(), $messageTag);
        $this->appendWithCData('n', $message->getFromName(), $messageTag);
        $this->appendWithCData('k', $message->getKey(), $messageTag);
        $this->appendWithCData('b', $message->getBlacklisted(), $messageTag);
        $this->appendWithCData('bm', $message->getBlacklistedMessage(), $messageTag);
        $this->appendWithCData('p', $pdfUrl . $message->getPdf(), $messageTag);
        
        $this->appendWithCData('s', $message->getSubject(), $messageTag);
        $this->appendWithCData('m', str_replace(array("<br />", "<br/>", "<br>"), "\n", preg_replace('/\p{C}+/u', '', nl2br($message->getMessage()))), $messageTag);
        $this->appendWithCData('d', date_format($message->getEmailDate(), 'c'), $messageTag);

        $attachmentsTag = $this->doc->createElement('as');
        $tAttribute = $this->doc->createAttribute('t');
        $tAttribute->value = $message->getAttachments()->count();
        $attachmentsTag->appendChild($tAttribute);
        
        $messageTag->appendChild($attachmentsTag);
        
        foreach ($message->getAttachments() as $attachment) {
            $attachmentTag = $this->doc->createElement('a');
            $this->appendWithCData('n', $attachment->getName(), $attachmentTag);
            $this->appendWithCData('m', $attachment->getMimeType(), $attachmentTag);
            $this->appendWithCData('s', $attachment->getSize(), $attachmentTag);
            $this->appendWithCData('u', $attachmentUrl . $attachment->getPath(), $attachmentTag);
            
            $attachmentsTag->appendChild($attachmentTag);
        }
        
        $this->rootTag->appendChild($messageTag);
    }
    
    protected function appendWithCData($name, $value, \DOMElement $parent)
    {
        $element = $this->doc->createElement($name);
        $cData   = $this->doc->createCDATASection($value);
        
        $element->appendChild($cData);
        $parent->appendChild($element);
    }
    
    
    public function getXML()
    {
        return $this->doc->saveXML($this->doc->documentElement);
    }
}