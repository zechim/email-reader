<?php 

namespace EReader\Core\XML;

use EReader\Repository\ERMessageRepository;
use EReader\Entity\ERMessage;

class MessageTransact
{
    protected $doc;
    protected $responseTag;
    
    public function __construct($xml, ERMessageRepository $erMessageRepository)
    {
        $xml = trim($xml);
        $xml = $xml ? $xml : '<empty></empty>';
        
        $this->doc = new \DOMEntity('1.0', 'UTF-8');
        $this->responseTag = $this->doc->createElement('r');
        $this->doc->appendChild($this->responseTag);
        
        try {
            $parser = new XMLParser();
            $parser->loadXML($xml);
            
            $t = (int) $parser->getValue('r', 't');
            
            if ($t <= 0) {
                throw new \Exception('Messages total not informed');
            }
            
            $messages = $parser->getNodes('/r/m');
            $messages = $messages ? $messages : array();
            
            $totalAttr = $this->doc->createAttribute('t');
            $totalAttr->value = is_array($messages) ? 0 : $messages->length;;
            
            $this->responseTag->appendChild($totalAttr);
            
            foreach ($messages as $message) {
                $id   = $message->getAttribute('id');
                $type = 'S';
                
                try {
                    $m = $this->doc->createElement('m');
                    
                    $erMessage = $erMessageRepository->find($id);
                    
                    if (!$erMessage) {
                        throw new \Exception(sprintf('Message not found', $id));
                    }
                    
                    if ($erMessage->getRead() == ERMessage::READ_YES) {
                        throw new \Exception(sprintf('Message already transacted', $id));
                    }

                    $erMessageRepository->setAsRead($erMessage);
                    
                } catch (\Exception $e) {
                    $m    = $this->doc->createElement('m', $e->getMessage());
                    $type = 'E';
                }
                
                $typeAttribute = $this->doc->createAttribute('t');
                $typeAttribute->value = $type;
                
                $idAttr = $this->doc->createAttribute('id');
                $idAttr->value = $id;
                
                $m->appendChild($idAttr);
                $m->appendChild($typeAttribute);
                
                $this->responseTag->appendChild($m);
            }
            
        } catch (\Exception $e) {
            $error = $this->doc->createElement('e');
            $cData = $this->doc->createCDATASection($e->getMessage());
            $error->appendChild($cData);
            
            $this->responseTag->appendChild($error);
        }
    }

    public function getXML()
    {
        return $this->doc->saveXML($this->doc->documentElement);
    }
}