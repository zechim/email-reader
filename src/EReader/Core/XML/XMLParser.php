<?php 

namespace EReader\Core\XML;

class XMLParser
{

    private $doc;

    public function __construct()
    {
        $this->loadXML('<__empty__></__empty__>');
    }

    public function getValue($node = '', $attr = null)
    {
        $node = $this->getNode($node, $attr);

        return $node instanceof \DOMNode ? trim($node->nodeValue) : null;
    }

    public function getNode($node = '', $attr = null)
    {
        $attr = ltrim($attr, '/@');
        $node = $this->doc->query('/' . rtrim(ltrim($node, '/'), '/') . ($attr ? '/@' . $attr : null));

        return $node instanceof \DOMNodeList ? $node->item(0) : null;
    }

    public function getNodes($node = '')
    {
        return $this->doc->query('/' . rtrim(ltrim($node, '/'), '/'));
    }

    public function loadXML($xml)
    {
        libxml_clear_errors();
        libxml_use_internal_errors(true);

        $doc = new \DOMDocument('1.0', 'UTF-8');

        try {
            $doc->loadXML($xml);

            $this->doc = new \DOMXPath($doc);

            $e = libxml_get_last_error();

            if ($e instanceof \LibXMLError) {
                throw new \Exception($e->message);
            }

        } catch (\Exception $e) {
            throw new \BadMethodCallException('Invalid XML [' . $e->getMessage() . ']');
        }
    }

    public function getXML()
    {
        return $this->doc->query('/')->item(0)->C14N();
    }

}
