<?php

namespace EReader\Entity;

/**
 * ERBlacklist
 */
class ERBlacklist
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $match;

    /**
     * @var boolean
     */
    private $body;

    /**
     * @var boolean
     */
    private $email;

    /**
     * @var boolean
     */
    private $subject;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $emails;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->emails = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return ERBlacklist
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set match
     *
     * @param string $match
     * @return ERBlacklist
     */
    public function setMatch($match)
    {
        $this->match = $match;
    
        return $this;
    }

    /**
     * Get match
     *
     * @return string 
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * Set body
     *
     * @param boolean $body
     * @return ERBlacklist
     */
    public function setBody($body)
    {
        $this->body = $body;
    
        return $this;
    }

    /**
     * Get body
     *
     * @return boolean 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set email
     *
     * @param boolean $email
     * @return ERBlacklist
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return boolean 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set subject
     *
     * @param boolean $subject
     * @return ERBlacklist
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return boolean 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Add emails
     *
     * @param \EReader\Entity\EREmail $emails
     * @return ERBlacklist
     */
    public function addEmail(\EReader\Entity\EREmail $emails)
    {
        $this->emails[] = $emails;
    
        return $this;
    }

    /**
     * Remove emails
     *
     * @param \EReader\Entity\EREmail $emails
     */
    public function removeEmail(\EReader\Entity\EREmail $emails)
    {
        $this->emails->removeElement($emails);
    }

    /**
     * Get emails
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmails()
    {
        return $this->emails;
    }
    /**
     * @var string
     */
    private $name;


    /**
     * Set name
     *
     * @param string $name
     * @return ERBlacklist
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}