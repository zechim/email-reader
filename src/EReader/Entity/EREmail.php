<?php

namespace EReader\Entity;

/**
 * EREmail
 */
class EREmail
{
    const STATUS_WAITING = 'WAITING';
    const STATUS_RUNNING = 'RUNNING';
    
    const ACTIVE_NO  = 0;
    const ACTIVE_YES = 1;
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $account;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $pass;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $lastFetchedAt;

    /**
     * @var integer
     */
    private $lastFetchedTotal;

    /**
     * @var string
     */
    private $lastMessage;

    /**
     * @var \DateTime
     */
    private $startedAt;

    /**
     * @var string
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set account
     *
     * @param string $account
     * @return EREmail
     */
    public function setAccount($account)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return string 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set user
     *
     * @param string $user
     * @return EREmail
     */
    public function setUser($user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return string 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set pass
     *
     * @param string $pass
     * @return EREmail
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    
        return $this;
    }

    /**
     * Get pass
     *
     * @return string 
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return EREmail
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set lastFetchedAt
     *
     * @param \DateTime $lastFetchedAt
     * @return EREmail
     */
    public function setLastFetchedAt($lastFetchedAt)
    {
        $this->lastFetchedAt = $lastFetchedAt;
    
        return $this;
    }

    /**
     * Get lastFetchedAt
     *
     * @return \DateTime 
     */
    public function getLastFetchedAt()
    {
        return $this->lastFetchedAt;
    }

    /**
     * Set lastFetchedTotal
     *
     * @param integer $lastFetchedTotal
     * @return EREmail
     */
    public function setLastFetchedTotal($lastFetchedTotal)
    {
        $this->lastFetchedTotal = $lastFetchedTotal;
    
        return $this;
    }

    /**
     * Get lastFetchedTotal
     *
     * @return integer 
     */
    public function getLastFetchedTotal()
    {
        return $this->lastFetchedTotal;
    }

    /**
     * Set lastMessage
     *
     * @param string $lastMessage
     * @return EREmail
     */
    public function setLastMessage($lastMessage)
    {
        $this->lastMessage = $lastMessage;
    
        return $this;
    }

    /**
     * Get lastMessage
     *
     * @return string 
     */
    public function getLastMessage()
    {
        return $this->lastMessage;
    }

    /**
     * Set startedAt
     *
     * @param \DateTime $startedAt
     * @return EREmail
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;
    
        return $this;
    }

    /**
     * Get startedAt
     *
     * @return \DateTime 
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return EREmail
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var string
     */
    private $hostname;


    /**
     * Set hostname
     *
     * @param string $hostname
     * @return EREmail
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname;
    
        return $this;
    }

    /**
     * Get hostname
     *
     * @return string 
     */
    public function getHostname()
    {
        return $this->hostname;
    }
    /**
     * @var string
     */
    private $protocol;


    /**
     * Set protocol
     *
     * @param string $protocol
     * @return EREmail
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    
        return $this;
    }

    /**
     * Get protocol
     *
     * @return string 
     */
    public function getProtocol()
    {
        return $this->protocol;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $blacklist;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->blacklist = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add blacklist
     *
     * @param \EReader\Entity\ERBlacklist $blacklist
     * @return EREmail
     */
    public function addBlacklist(\EReader\Entity\ERBlacklist $blacklist)
    {
        $this->blacklist[] = $blacklist;
    
        return $this;
    }

    /**
     * Remove blacklist
     *
     * @param \EReader\Entity\ERBlacklist $blacklist
     */
    public function removeBlacklist(\EReader\Entity\ERBlacklist $blacklist)
    {
        $this->blacklist->removeElement($blacklist);
    }

    /**
     * Get blacklist
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBlacklist()
    {
        return $this->blacklist;
    }
    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $column1;

    /**
     * @var int
     */
    private $column2;


    /**
     * Set description
     *
     * @param string $description
     * @return EREmail
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set column1
     *
     * @param \int $column1
     * @return EREmail
     */
    public function setColumn1(\int $column1)
    {
        $this->column1 = $column1;
    
        return $this;
    }

    /**
     * Get column1
     *
     * @return \int 
     */
    public function getColumn1()
    {
        return $this->column1;
    }

    /**
     * Set column2
     *
     * @param \int $column2
     * @return EREmail
     */
    public function setColumn2(\int $column2)
    {
        $this->column2 = $column2;
    
        return $this;
    }

    /**
     * Get column2
     *
     * @return \int 
     */
    public function getColumn2()
    {
        return $this->column2;
    }
}