<?php

namespace EReader\Entity;

/**
 * EReader\Entity\ERMessage
 */
class ERMessage
{
    const READ_YES = 1;
    const READ_NO  = 0;
    
    /**
     * @var MongoId $id
     */
    protected $id;

    /**
     * @var string $subject
     */
    protected $subject;

    /**
     * @var string $fromEmail
     */
    protected $fromEmail;

    /**
     * @var string $fromName
     */
    protected $fromName;

    /**
     * @var string $message
     */
    protected $message;

    /**
     * @var string $fetchedBy
     */
    protected $fetchedBy;

    /**
     * @var string $key
     */
    protected $key;

    /**
     * @var int $read
     */
    protected $read;

    /**
     * @var date $emailDate
     */
    protected $emailDate;

    /**
     * @var EReader\Entity\ERMessageAttachment
     */
    protected $attachments = array();

    public function __construct()
    {
        $this->attachments = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return self
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Get subject
     *
     * @return string $subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set fromEmail
     *
     * @param string $fromEmail
     * @return self
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;
        return $this;
    }

    /**
     * Get fromEmail
     *
     * @return string $fromEmail
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * Set fromName
     *
     * @param string $fromName
     * @return self
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;
        return $this;
    }

    /**
     * Get fromName
     *
     * @return string $fromName
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get message
     *
     * @return string $message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set fetchedBy
     *
     * @param string $fetchedBy
     * @return self
     */
    public function setFetchedBy($fetchedBy)
    {
        $this->fetchedBy = $fetchedBy;
        return $this;
    }

    /**
     * Get fetchedBy
     *
     * @return string $fetchedBy
     */
    public function getFetchedBy()
    {
        return $this->fetchedBy;
    }

    /**
     * Set key
     *
     * @param string $key
     * @return self
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * Get key
     *
     * @return string $key
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set read
     *
     * @param int $read
     * @return self
     */
    public function setRead($read)
    {
        $this->read = $read;
        return $this;
    }

    /**
     * Get read
     *
     * @return int $read
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * Set emailDate
     *
     * @param date $emailDate
     * @return self
     */
    public function setEmailDate($emailDate)
    {
        $this->emailDate = $emailDate;
        return $this;
    }

    /**
     * Get emailDate
     *
     * @return date $emailDate
     */
    public function getEmailDate()
    {
        return $this->emailDate;
    }

    /**
     * Add attachments
     *
     * @param EReader\Entity\ERMessageAttachment $attachments
     */
    public function addAttachment(\EReader\Entity\ERMessageAttachment $attachments)
    {
        $attachments->setMessage($this);

        $this->attachments[] = $attachments;
    }

    /**
    * Remove attachments
    *
    * @param <variableType$attachments
    */
    public function removeAttachment(\EReader\Entity\ERMessageAttachment $attachments)
    {
        $this->attachments->removeElement($attachments);
    }

    /**
     * Get attachments
     *
     * @return Doctrine\Common\Collections\Collection $attachments
     */
    public function getAttachments()
    {
        return $this->attachments;
    }
    /**
     * @var int $blacklisted
     */
    protected $blacklisted;

    /**
     * @var string $blacklistedMessage
     */
    protected $blacklistedMessage;


    /**
     * Set blacklisted
     *
     * @param int $blacklisted
     * @return self
     */
    public function setBlacklisted($blacklisted)
    {
        $this->blacklisted = $blacklisted;
        return $this;
    }

    /**
     * Get blacklisted
     *
     * @return int $blacklisted
     */
    public function getBlacklisted()
    {
        return $this->blacklisted;
    }

    /**
     * Set blacklistedMessage
     *
     * @param string $blacklistedMessage
     * @return self
     */
    public function setBlacklistedMessage($blacklistedMessage)
    {
        $this->blacklistedMessage = $blacklistedMessage;
        return $this;
    }

    /**
     * Get blacklistedMessage
     *
     * @return string $blacklistedMessage
     */
    public function getBlacklistedMessage()
    {
        return $this->blacklistedMessage;
    }
    /**
     * @var string $pdf
     */
    protected $pdf;


    /**
     * Set pdf
     *
     * @param string $pdf
     * @return self
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
        return $this;
    }

    /**
     * Get pdf
     *
     * @return string $pdf
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return ERMessage
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }
}