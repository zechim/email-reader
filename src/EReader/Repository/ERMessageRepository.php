<?php

namespace EReader\Repository;

use EReader\Entity\ERMessage;
use EReader\Entity\EREmail;
use EReader\Entity\ERMessageAttachment;
use EReader\Core\Fetcher\DispatcherConfig;
use EReader\Core\Protocol\ImapMessage;
use Doctrine\ORM\EntityRepository;

/**
 * ERMessageRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ERMessageRepository extends EntityRepository
{
    public function getMatch($regex, $text)
    {
        $output = array();

        preg_match($regex, $text, $output);
        
        $key = isset($output[0]) ? $output[0] : '';

        return trim($key);
    }
    
    public function findKeyOnMessage(ImapMessage $message, $regex)
    {
        $key = $this->getMatch($regex, $message->getHeader()->getSubject());
        
        if ($key == '') {
            $key = $this->getMatch($regex, $message->getBodyText());
        }
        
        return $key;
    }
    
    public function setAsRead(ERMessage $erMessage)
    {
        $erMessage->setRead(ERMessage::READ_YES);

        $this->_em->persist($erMessage);
        $this->_em->flush();
    }
    
    public function saveMessage(ImapMessage $message, EREmail $erEmail, DispatcherConfig $dispatcherConfig)
    {
        $erMessage = new ERMessage();
        $erMessage->setEmailDate($message->getHeader()->getDate());
        $erMessage->setFromEmail($message->getHeader()->getFromEmail());
        $erMessage->setFromName($message->getHeader()->getFromName());
        $erMessage->setSubject($message->getHeader()->getSubject());
        $erMessage->setMessage($message->getBodyText());
        $erMessage->setKey($this->findKeyOnMessage($message, $dispatcherConfig->getHashRegex()));
        $erMessage->setRead(ERMessage::READ_NO);
        $erMessage->setFetchedBy($erEmail->getUser());
        $erMessage->setPdf($message->savePDF($dispatcherConfig->getPdfPath(), $dispatcherConfig->getPdfUrl()));
        $erMessage->setBlacklisted(0);

        foreach ($erEmail->getBlacklist() as $blacklist) {
            if ($blacklist->getActive() != 'Y') {
                continue;
            } 
            
            $messageList = array();
            $regex = sprintf('/%s/i', $blacklist->getMatch());
            
            if ($blacklist->getBody() && ($this->getMatch($regex, $message->getBodyText()) !== '' | $this->getMatch($regex, $message->getBodyHtml()) !== '')) {
                $erMessage->setBlacklisted(1);
                $messageList[] = sprintf('Found [%s] on %s [rule %d]',  $blacklist->getMatch(), 'body', $blacklist->getId());
            }   
                
            if ($blacklist->getSubject() && $this->getMatch($regex, $message->getHeader()->getSubject()) !== '') {
                $erMessage->setBlacklisted(1);
                $messageList[] = sprintf('Found [%s] on %s [rule %d]',  $blacklist->getMatch(), 'subject', $blacklist->getId());
            }
                
            if ($blacklist->getEmail() && $this->getMatch($regex, $message->getHeader()->getFromEmail()) !== '') {
                $erMessage->setBlacklisted(1);
                $messageList[] = sprintf('Found [%s] on %s [rule %d]',  $blacklist->getMatch(), 'email', $blacklist->getId());
            }
            
            if ($blacklist->getName() && $this->getMatch($regex, $message->getHeader()->getFromName()) !== '') {
                $erMessage->setBlacklisted(1);
                $messageList[] = sprintf('Found [%s] on %s [rule %d]',  $blacklist->getMatch(), 'name', $blacklist->getId());
            }
                
            if (count($messageList)) {
                $erMessage->setBlacklistedMessage(implode(', ', $messageList));
            }
        }
        
        /* @var $message \EReader\Core\Protocol\ImapMessageAttachment */
        foreach ($message->getAttachments() as $attachment) {
            $erMessageAttachment = new ERMessageAttachment();
            $erMessageAttachment->setMimeType($attachment->getType());
            $erMessageAttachment->setName($attachment->getFilename());
            $erMessageAttachment->setSize($attachment->getSize());
            $erMessageAttachment->setPath($attachment->saveFile($dispatcherConfig->getAttachmentPath()));
            $erMessageAttachment->setCreatedAt(new \DateTime);
            
            $erMessage->addAttachment($erMessageAttachment);
            $this->_em->persist($erMessageAttachment);
        } 
        
        $this->_em->persist($erMessage);
        $this->_em->flush();
    }
    
    public function findLike($like, $blacklisted, $limit)
    {
        $qb = $this->createQueryBuilder('m');
        
        $or = $qb->expr()->orX();
        
        foreach (array('message', 'subject', 'key', 'fromName', 'fromEmail') as $column) {
            $or->add($qb->expr()->like("m.$column", $qb->expr()->literal(sprintf('%%%s%%', $like))));
        }
        
        $qb->andWhere($or);
        $qb->andWhere("m.blacklisted = $blacklisted");
        $qb->setMaxResults($limit);

        return $qb->getQuery()->execute();
    }
}