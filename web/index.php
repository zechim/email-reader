<?php

require_once '../bootstrap.php';

use EReader\Entity\ERMessage;
use EReader\Core\XML\MessageFetcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use EReader\Core\XML\MessageTransact;

$app->get('/getMessages', function(Request $request) use ($app) {
    $like = trim($request->query->get('like'));
    $limit = $request->query->get('limit', 10);
    $blacklisted = (int) $request->query->get('blacklisted', 0);
    
    $repo = $app['db.orm.em']->getRepository('EReader\Entity\ERMessage');
    
    if ($like != '') {
        $messages = $repo->findLike($like, $blacklisted, $limit);
        
    } else {
        $messages = $repo->findBy(array('read' => ERMessage::READ_NO, 'blacklisted' => $blacklisted), null, $limit);
    }
    
    $messages = new MessageFetcher($messages, $app['config']);
        
    return new Response($messages->getXML(), 200, array('Content-Type' => 'text/xml'));
});

$app->match('/transactMessages', function(Request $request) use ($app) {
    $transact = new MessageTransact($request->request->get('xml'), $app['db.orm.em']->getRepository('EReader\Entity\ERMessage'));

    return new Response($transact->getXML(), 200, array('Content-Type' => 'text/xml'));
});

$app->run();
